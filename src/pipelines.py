import os
import pypandoc
import bleach
import hashlib
import furl

from src.constants import ALLOWED_TAGS
from src.schema.models.items import Item, ItemComponent
from src.utils.filters import NoChildContentCleaner

from scrapy.pipelines.images import ImagesPipeline
from scrapy.pipelines.files import FilesPipeline
from scrapy.utils.python import to_bytes


class ImagePipeline(ImagesPipeline):
    def file_path(self, request, response=None, info=None):
        
        # strip querystring params
        f = furl.furl(request.url)
        f.query = None
        
        # split into useful string components
        image_guid = hashlib.sha1(to_bytes(f.url)).hexdigest()
        image_ext = os.path.splitext(f.url)[1]
        job_id = self.spiderinfo.spider._job.identifier.hex
        return '%s/images/%s%s' % (job_id, image_guid, image_ext)
    
    
class FilePipeline(FilesPipeline):
    def file_path(self, request, response=None, info=None):
        
        # strip querystring params
        f = furl.furl(request.url)
        f.query = None
        
        # split into useful string components
        media_guid = hashlib.sha1(to_bytes(f.url)).hexdigest()
        media_ext = os.path.splitext(f.url)[1]
        job_id = self.spiderinfo.spider._job.identifier.hex
        
        return '%s/files/%s%s' % (job_id, media_guid, media_ext)


class AssignEncodingPipeline(object):
    
    def process_item(self, item, spider):
        item['encoding'] = item['response'].encoding
        return item


class ApplyRawBytesResponsePipeline(object):
    """
        Checks each processed item and figures out if the processing
        spider has an instantiated extractor, calling the extract method
        and mutating the Item
    """
    
    def process_item(self, item, spider):
        item['raw_body'] = item['response'].body
        return item


class DecodeResponseBodyFromBytesPipeline(object):
    """
        Checks each processed item and figures out if the processing
        spider has an instantiated extractor, calling the extract method
        and mutating the Item
    """
    
    def process_item(self, item, spider):
        item['string_body'] = item['response'].body_as_unicode()
        item['string_body'] = item['string_body'].replace('\n', '').replace('\t', '').replace('\r', '')
        return item


class ExtractBodyDataBlock(object):
    
    def process_item(self, item, spider):
        if spider.extractor:
            spider.extractor(item=item, spider=spider).extract()
            #spider.extractor.extract()
        return item


class CleanBodyDataBlock(object):
    
    def process_item(self, item, spider):
        if item.get('body_data_block', None) is not None:
            stripped_data = NoChildContentCleaner().clean(item['body_data_block'])
            item['cleaned_data_block'] = bleach.sanitizer.Cleaner(
                tags=ALLOWED_TAGS,
                strip=True,
                strip_comments=True,
            ).clean(stripped_data)
        return item


class ProcessDataBlock(object):
    
    def process_item(self, item, spider):
        if item.get('cleaned_data_block', None) is not None:
            item['processed_data_block'] = pypandoc.convert(
                source=item['cleaned_data_block'],
                to='md',
                format='html',
                extra_args=['--atx-headers', ],
            )
        return item


class AssignForeignKeysPipeline(object):
    def process_item(self, item, spider):
        item['party_id'] = spider._party.id
        item['region_id'] = spider._region.id
        item['job_id'] = spider._job.id
        item['href_id'] = spider.get_href(item['response']).id
        return item


class DatabaseRecordPipeline(object):
    """
        Checks each processed item and figures out if the processing
        spider has an instantiated extractor, calling the extract method
        and mutating the Item
    """
    
    def create_component_records(self, dbitem, spider):
        return [ItemComponent(item=dbitem, name=component[0], content=component[1], job_id=spider._job.id) for component
                in spider.extractor.extracted_list if spider.extractor and spider.extractor.extracted_list]
    
    def create_database_item(self, item):
        return Item(**{k: v for k, v in item.items() if k in Item.__table__.columns.keys()})
    
    def process_item(self, item, spider):
        dbitem = self.create_database_item(item)
        spider.session.add(dbitem)
        
        components = self.create_component_records(dbitem, spider)
        spider.session.add_all(components)
        return item
