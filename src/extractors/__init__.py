import abc
import bs4
import dateutil
import attr
import itertools

# from src.spiders.base import Item
from breadability.readable import Article


class BaseExtractor(abc.ABC, metaclass=abc.ABCMeta):
    
    @abc.abstractmethod
    def extract(self, item):
        """
        Mandatory method for extraction.  Takes the item object in and performs
        mutations on the keys available.
        """


@attr.s
class DefaultExtractor(BaseExtractor):
    TITLE_META = [
        'og:title',
        'twitter:title',
    ]
    
    DESCRIPTION_META = [
        'twitter:description',
        'og:description',
    ]
    
    IMAGE_META = [
        'og:image',
        'twitter:image',
    ]
    
    PUBLISHER_META = [
        'article:author',
        'article:publisher',
    ]
    
    item = attr.ib()
    spider = attr.ib()
    
    extracted_list = []
    
    def extract_meta(self):
        for meta in itertools.chain.from_iterable([self.TITLE_META, self.DESCRIPTION_META, self.IMAGE_META, self.PUBLISHER_META]):
            data = self.soup.find('meta', attrs={'property': meta})
            if data:
                self.extracted_list.append((data['property'].strip(), data['content'].strip()))
    
    def extract_their_publish_date(self, item):
        node = self.soup.find('meta', property='article:published_time')
        if node:
            content = node['content']
            return dateutil.parser.parse(content)
        return None
    
    def extract(self):
        response = self.item['response']
        self.article = Article(html=self.item['string_body'], url=response.url)
        self.soup = bs4.BeautifulSoup(self.item['string_body'], 'lxml')
        self.item['body_data_block'] = self.article.readable
        # self.item['title'] = self.soup.find("meta", property="og:title")['content']
        # self.item['their_publish_date'] = self.extract_their_publish_date(self.item)
        
        self.extract_meta()
