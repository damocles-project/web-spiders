from __future__ import annotations
import typing
import uuid
import pytz
import datetime

import sqlalchemy as sa
from sqlalchemy import text
from sqlalchemy.orm import relationship, backref, aliased
from sqlalchemy.dialects.postgresql import UUID, JSONB, TIMESTAMP, TEXT
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy import (
    MetaData,
    Column,
    ForeignKey,
    Integer,
    DateTime,
    String,
    UniqueConstraint,
    Boolean,
)
from sqlalchemy_utils import generic_repr, generic_relationship, UUIDType
from sqlalchemy_utils.models import Timestamp

convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}


metadata = MetaData(schema=None, naming_convention=convention)
Base = declarative_base(metadata=metadata)




GEN_RANDOM_UUID = text("gen_random_uuid()")
NULL = text("NULL")
TRUE = text("TRUE")
FALSE = text("FALSE")
NOW = text("now()")



class TableBase(Base):
    __abstract__ = True

    id = Column(
        Integer,
        primary_key=True,
        nullable=False,
        unique=True,
        index=True,
    )
    identifier = Column(
        UUID(as_uuid=True),
        unique=True,
        index=True,
        nullable=False,
        server_default=GEN_RANDOM_UUID,
    )
    date_created = Column(
        TIMESTAMP,
        nullable=False,
        unique=False,
        index=True,
        server_default=NOW,
    )
    date_modified = Column(
        TIMESTAMP,
        nullable=False,
        unique=False,
        index=True,
        server_default=NOW,
    )

    @declared_attr
    def __foreign_key__(self):
        return f"{self.__tablename__}.id"


class ResourceType(TableBase):
    __tablename__ = "resource_type"

    name = Column(
        String(255),
        nullable=False,
        unique=True,
        index=True,
    )



class Resource(TableBase):
    __tablename__ = "resource"
    __table_args__ = (
        UniqueConstraint(
            "resource_type_id",
            "object_id",
            name="resource_type_id__object_id__unique_together",
        ),
    )

    resource_type_id = Column(
        Integer,
        ForeignKey(ResourceType.__foreign_key__),
        nullable=False,
        index=True,
    )
    object_id = Column(
        Integer,
        nullable=False,
        index=True,
        unique=False,
    )
    is_archived = Column(
        Boolean,
        server_default=FALSE,
        index=True,
    )
    date_archived = Column(
        TIMESTAMP,
        nullable=True,
        index=True,
        server_default=NULL,
    )

    # This is used to discriminate between the linked tables.
    object_type = sa.Column(sa.Unicode(255))
    
    # This is used to point to the primary key of the linked row.
    object_id = sa.Column(sa.Integer)
    
    object = generic_relationship(object_type, object_id)
    resource_history = relationship(lambda: ResourceHistory)
    resource_type = relationship(lambda: ResourceType)

    def archive(self) -> Resource:
        self.is_archived = True
        return self


class ResourceHistory(TableBase):
    __tablename__ = "resource_history"

    resource_id = Column(
        Integer,
        ForeignKey(Resource.__foreign_key__),
        nullable=False,
        index=True,
    )
    date_archived = Column(
        TIMESTAMP,
        default=None,
        nullable=True,
        index=True,
    )
    resource_data = Column(
        JSONB,
        index=True,
        nullable=False,
        server_default=NULL,
    )

    resource = relationship(Resource, lazy="joined")


@generic_repr
class ModelBase(TableBase):
    __abstract__ = True

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    @declared_attr
    def resource_id(self):
        return Column(
            Integer,
            ForeignKey(Resource.__foreign_key__),
            nullable=False,
            index=True,
        )

    @declared_attr
    def resource(self) -> Resource:
        return relationship(Resource)
