import sqlalchemy as sa
from sqlalchemy.orm import relationship
from .base import ModelBase
from .through import ItemMemberThrough

__all__ = ['Domain', 'HREF', 'Item', 'ItemComponent', 'Job', ]


class Job(ModelBase):
    spider_name = sa.Column(
        type_=sa.String(100),
        index=True,
        nullable=False,
        comment='The name of the spider that ran this job'
    )
    start_time = sa.Column(
        type_=sa.DateTime(timezone=True),
        index=True,
        nullable=True,
        default=None,
    )
    finish_time = sa.Column(
        type_=sa.DateTime(timezone=True),
        index=True,
        nullable=True,
        default=None,
    )
    info = sa.Column(
        sa.JSON,
        nullable=True,
        default=None,
    )


class HREF(ModelBase):
    url = sa.Column(
        sa.String,
        index=True,
        nullable=False,
        unique=True,
    )
    domain_id = sa.Column(
        sa.Integer,
        sa.ForeignKey('domain.id'),
        nullable=False,
        index=True,
    )
    
    item_set = relationship(
        'schema.models.items.Item',
    )


class Domain(ModelBase):
    subdomain = sa.Column(
        sa.String(500),
        index=True,
        nullable=True,
        default=None,
        unique=False,
    )
    domain = sa.Column(
        sa.String(500),
        index=True,
        nullable=False,
        unique=False,
    )
    suffix = sa.Column(
        sa.String(500),
        index=True,
        nullable=False,
        default=None,
        unique=False,
    )


class Item(ModelBase):
    # Attributes
    encoding = sa.Column(
        sa.String(30),
        index=True,
        nullable=False,
        comment="Has this data been processed into a generic serialization format?"
    )
    title = sa.Column(
        sa.String(500),
        index=True,
        nullable=True,
        default=None,
        comment='The title of the page to be displayed to the end user'
    )
    their_publish_date = sa.Column(
        type_=sa.Date,
        nullable=True,
        index=True,
        comment='The extracted date from their page that this was published on'
    )
    raw_body = sa.Column(
        type_=sa.LargeBinary,
        nullable=False,
        comment='The raw HTML response body in bytes'
    )
    string_body = sa.Column(
        type_=sa.Text,
        nullable=False,
        comment='The raw HTML body stringified'
    )
    body_data_block = sa.Column(
        sa.Text,
        nullable=True,
        default=None,
        comment='The body block to be processed'
    )
    cleaned_data_block = sa.Column(
        sa.Text,
        nullable=True,
        default=None,
        comment='Preprocessed content'
    )
    processed_data_block = sa.Column(
        sa.Text,
        nullable=True,
        default=None,
        comment='The processed Markdown body'
    )
    processed = sa.Column(
        sa.Boolean,
        default=False,
        index=True,
        nullable=False,
        comment="Has this data been processed into a generic serialization format?"
    )
    
    # relationships
    job_id = sa.Column(
        sa.Integer,
        sa.ForeignKey('job.id'),
        nullable=False,
        index=True,
    )
    party_id = sa.Column(
        sa.Integer,
        sa.ForeignKey('party.id'),
        nullable=False,
        index=True,
    )
    region_id = sa.Column(
        sa.Integer,
        sa.ForeignKey('region.id'),
        nullable=False,
        index=True,
    )
    href_id = sa.Column(
        sa.Integer,
        sa.ForeignKey('href.id'),
        nullable=False,
        index=True,
    )
    
    # computed backrefs
    region = relationship(
        'schema.models.locations.Region',
        back_populates='item_set'
    )
    party = relationship(
        'schema.models.agents.Party',
        back_populates='item_set'
    )
    href = relationship(
        'schema.models.items.HREF',
        back_populates='item_set',
    )
    member_set = relationship(
        'schema.models.agents.Member',
        back_populates='item_set',
        secondary=ItemMemberThrough.__table__,
    )
    # meta_set = relationship(
    #     'schema.models.item.ItemComponent',
    # )


class ItemComponent(ModelBase):
    item_id = sa.Column(
        sa.Integer,
        sa.ForeignKey('item.id'),
        nullable=False,
        index=True,
    )
    
    job_id = sa.Column(
        sa.Integer,
        sa.ForeignKey('job.id'),
        nullable=False,
        index=True,
    )
    
    name = sa.Column(
        sa.String(50),
        nullable=False,
        index=True,
    )
    
    content = sa.Column(
        sa.Text,
        nullable=False,
        index=True,
    )
    
    item = relationship(
        'schema.models.items.Item',
        # back_populates='meta_set',
    )
    #
    # job = relationship(
    #     'schema.models.items.Job',
    #
    # )
