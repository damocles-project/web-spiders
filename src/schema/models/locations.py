from sqlalchemy.orm import relationship
from sqlalchemy import (
    Column,
    String,
)
from .base import ModelBase

__all__ = ['Region', ]


class Region(ModelBase):
    name = Column(
        type_=String(length=30),
        unique=True,
        nullable=False,
    )
    
    item_set = relationship(
        'schema.models.items.Item',
    )
