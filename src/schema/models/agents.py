import sqlalchemy as sa
from sqlalchemy.orm import relationship
from .base import ModelBase
from .through import ItemMemberThrough

__all__ = ['Party', 'Member', ]


class Party(ModelBase):
    name = sa.Column(
        sa.String(length=200),
        unique=True,
        index=True,
    )
    
    item_set = relationship('schema.models.items.Item', )


class Member(ModelBase):
    first_name = sa.Column(
        sa.String(length=500),
        index=True,
        nullable=False,
    )
    
    last_name = sa.Column(
        sa.String(length=500),
        index=True,
        nullable=False,
    )
    
    date_of_birth = sa.Column(
        sa.Date,
        index=True,
        nullable=False,
    )
    
    item_set = relationship(
        'schema.models.items.Item',
        back_populates='member_set',
        secondary=ItemMemberThrough.__table__,
    )
