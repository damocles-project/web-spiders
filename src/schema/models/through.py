import sqlalchemy as sa
from .base import ModelBase

__all__ = ['ItemMemberThrough', ]


class ItemMemberThrough(ModelBase):
    __tablename__ = 'item_member_through'
    __table_args__ = (
        sa.UniqueConstraint('item_id', 'member_id', name='uix_item_id_member_id_%s' % __tablename__),
        sa.Index('item_id', 'member_id'),
        sa.Index('member_id', 'item_id'),
    )
    
    item_id = sa.Column(
        sa.Integer,
        sa.ForeignKey('item.id'),
        nullable=False,
        index=True,
    )
    member_id = sa.Column(
        sa.Integer,
        sa.ForeignKey('member.id'),
        nullable=False,
        index=True,
    )
