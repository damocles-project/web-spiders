logging.getLogger('parso.python.diff').disabled = True
# https://github.com/ipython/ipython/issues/10946
{'parso': { 'handlers': ['console'], 'level': 'INFO', 'propagate': False, },}