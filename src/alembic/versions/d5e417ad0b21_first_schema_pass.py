"""First schema pass

Revision ID: d5e417ad0b21
Revises: 
Create Date: 2022-08-30 08:35:50.985735

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'd5e417ad0b21'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('resource_type',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('name', sa.String(length=255), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_resource_type'))
    )
    op.create_index(op.f('ix_resource_type_date_created'), 'resource_type', ['date_created'], unique=False)
    op.create_index(op.f('ix_resource_type_date_modified'), 'resource_type', ['date_modified'], unique=False)
    op.create_index(op.f('ix_resource_type_id'), 'resource_type', ['id'], unique=True)
    op.create_index(op.f('ix_resource_type_identifier'), 'resource_type', ['identifier'], unique=True)
    op.create_index(op.f('ix_resource_type_name'), 'resource_type', ['name'], unique=True)
    op.create_table('resource',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('resource_type_id', sa.Integer(), nullable=False),
    sa.Column('is_archived', sa.Boolean(), server_default=sa.text('FALSE'), nullable=True),
    sa.Column('date_archived', postgresql.TIMESTAMP(), server_default=sa.text('NULL'), nullable=True),
    sa.Column('object_type', sa.Unicode(length=255), nullable=True),
    sa.Column('object_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['resource_type_id'], ['resource_type.id'], name=op.f('fk_resource_resource_type_id_resource_type')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_resource')),
    sa.UniqueConstraint('resource_type_id', 'object_id', name='resource_type_id__object_id__unique_together')
    )
    op.create_index(op.f('ix_resource_date_archived'), 'resource', ['date_archived'], unique=False)
    op.create_index(op.f('ix_resource_date_created'), 'resource', ['date_created'], unique=False)
    op.create_index(op.f('ix_resource_date_modified'), 'resource', ['date_modified'], unique=False)
    op.create_index(op.f('ix_resource_id'), 'resource', ['id'], unique=True)
    op.create_index(op.f('ix_resource_identifier'), 'resource', ['identifier'], unique=True)
    op.create_index(op.f('ix_resource_is_archived'), 'resource', ['is_archived'], unique=False)
    op.create_index(op.f('ix_resource_resource_type_id'), 'resource', ['resource_type_id'], unique=False)
    op.create_table('domain',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('subdomain', sa.String(length=500), nullable=True),
    sa.Column('domain', sa.String(length=500), nullable=False),
    sa.Column('suffix', sa.String(length=500), nullable=False),
    sa.Column('resource_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['resource_id'], ['resource.id'], name=op.f('fk_domain_resource_id_resource')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_domain'))
    )
    op.create_index(op.f('ix_domain_date_created'), 'domain', ['date_created'], unique=False)
    op.create_index(op.f('ix_domain_date_modified'), 'domain', ['date_modified'], unique=False)
    op.create_index(op.f('ix_domain_domain'), 'domain', ['domain'], unique=False)
    op.create_index(op.f('ix_domain_id'), 'domain', ['id'], unique=True)
    op.create_index(op.f('ix_domain_identifier'), 'domain', ['identifier'], unique=True)
    op.create_index(op.f('ix_domain_resource_id'), 'domain', ['resource_id'], unique=False)
    op.create_index(op.f('ix_domain_subdomain'), 'domain', ['subdomain'], unique=False)
    op.create_index(op.f('ix_domain_suffix'), 'domain', ['suffix'], unique=False)
    op.create_table('job',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('spider_name', sa.String(length=100), nullable=False, comment='The name of the spider that ran this job'),
    sa.Column('start_time', sa.DateTime(timezone=True), nullable=True),
    sa.Column('finish_time', sa.DateTime(timezone=True), nullable=True),
    sa.Column('info', sa.JSON(), nullable=True),
    sa.Column('resource_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['resource_id'], ['resource.id'], name=op.f('fk_job_resource_id_resource')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_job'))
    )
    op.create_index(op.f('ix_job_date_created'), 'job', ['date_created'], unique=False)
    op.create_index(op.f('ix_job_date_modified'), 'job', ['date_modified'], unique=False)
    op.create_index(op.f('ix_job_finish_time'), 'job', ['finish_time'], unique=False)
    op.create_index(op.f('ix_job_id'), 'job', ['id'], unique=True)
    op.create_index(op.f('ix_job_identifier'), 'job', ['identifier'], unique=True)
    op.create_index(op.f('ix_job_resource_id'), 'job', ['resource_id'], unique=False)
    op.create_index(op.f('ix_job_spider_name'), 'job', ['spider_name'], unique=False)
    op.create_index(op.f('ix_job_start_time'), 'job', ['start_time'], unique=False)
    op.create_table('member',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('first_name', sa.String(length=500), nullable=False),
    sa.Column('last_name', sa.String(length=500), nullable=False),
    sa.Column('date_of_birth', sa.Date(), nullable=False),
    sa.Column('resource_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['resource_id'], ['resource.id'], name=op.f('fk_member_resource_id_resource')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_member'))
    )
    op.create_index(op.f('ix_member_date_created'), 'member', ['date_created'], unique=False)
    op.create_index(op.f('ix_member_date_modified'), 'member', ['date_modified'], unique=False)
    op.create_index(op.f('ix_member_date_of_birth'), 'member', ['date_of_birth'], unique=False)
    op.create_index(op.f('ix_member_first_name'), 'member', ['first_name'], unique=False)
    op.create_index(op.f('ix_member_id'), 'member', ['id'], unique=True)
    op.create_index(op.f('ix_member_identifier'), 'member', ['identifier'], unique=True)
    op.create_index(op.f('ix_member_last_name'), 'member', ['last_name'], unique=False)
    op.create_index(op.f('ix_member_resource_id'), 'member', ['resource_id'], unique=False)
    op.create_table('party',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('name', sa.String(length=200), nullable=True),
    sa.Column('resource_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['resource_id'], ['resource.id'], name=op.f('fk_party_resource_id_resource')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_party'))
    )
    op.create_index(op.f('ix_party_date_created'), 'party', ['date_created'], unique=False)
    op.create_index(op.f('ix_party_date_modified'), 'party', ['date_modified'], unique=False)
    op.create_index(op.f('ix_party_id'), 'party', ['id'], unique=True)
    op.create_index(op.f('ix_party_identifier'), 'party', ['identifier'], unique=True)
    op.create_index(op.f('ix_party_name'), 'party', ['name'], unique=True)
    op.create_index(op.f('ix_party_resource_id'), 'party', ['resource_id'], unique=False)
    op.create_table('region',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('name', sa.String(length=30), nullable=False),
    sa.Column('resource_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['resource_id'], ['resource.id'], name=op.f('fk_region_resource_id_resource')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_region')),
    sa.UniqueConstraint('name', name=op.f('uq_region_name'))
    )
    op.create_index(op.f('ix_region_date_created'), 'region', ['date_created'], unique=False)
    op.create_index(op.f('ix_region_date_modified'), 'region', ['date_modified'], unique=False)
    op.create_index(op.f('ix_region_id'), 'region', ['id'], unique=True)
    op.create_index(op.f('ix_region_identifier'), 'region', ['identifier'], unique=True)
    op.create_index(op.f('ix_region_resource_id'), 'region', ['resource_id'], unique=False)
    op.create_table('resource_history',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('resource_id', sa.Integer(), nullable=False),
    sa.Column('date_archived', postgresql.TIMESTAMP(), nullable=True),
    sa.Column('resource_data', postgresql.JSONB(astext_type=sa.Text()), server_default=sa.text('NULL'), nullable=False),
    sa.ForeignKeyConstraint(['resource_id'], ['resource.id'], name=op.f('fk_resource_history_resource_id_resource')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_resource_history'))
    )
    op.create_index(op.f('ix_resource_history_date_archived'), 'resource_history', ['date_archived'], unique=False)
    op.create_index(op.f('ix_resource_history_date_created'), 'resource_history', ['date_created'], unique=False)
    op.create_index(op.f('ix_resource_history_date_modified'), 'resource_history', ['date_modified'], unique=False)
    op.create_index(op.f('ix_resource_history_id'), 'resource_history', ['id'], unique=True)
    op.create_index(op.f('ix_resource_history_identifier'), 'resource_history', ['identifier'], unique=True)
    op.create_index(op.f('ix_resource_history_resource_data'), 'resource_history', ['resource_data'], unique=False)
    op.create_index(op.f('ix_resource_history_resource_id'), 'resource_history', ['resource_id'], unique=False)
    op.create_table('href',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('url', sa.String(), nullable=False),
    sa.Column('domain_id', sa.Integer(), nullable=False),
    sa.Column('resource_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['domain_id'], ['domain.id'], name=op.f('fk_href_domain_id_domain')),
    sa.ForeignKeyConstraint(['resource_id'], ['resource.id'], name=op.f('fk_href_resource_id_resource')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_href'))
    )
    op.create_index(op.f('ix_href_date_created'), 'href', ['date_created'], unique=False)
    op.create_index(op.f('ix_href_date_modified'), 'href', ['date_modified'], unique=False)
    op.create_index(op.f('ix_href_domain_id'), 'href', ['domain_id'], unique=False)
    op.create_index(op.f('ix_href_id'), 'href', ['id'], unique=True)
    op.create_index(op.f('ix_href_identifier'), 'href', ['identifier'], unique=True)
    op.create_index(op.f('ix_href_resource_id'), 'href', ['resource_id'], unique=False)
    op.create_index(op.f('ix_href_url'), 'href', ['url'], unique=True)
    op.create_table('item',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('encoding', sa.String(length=30), nullable=False, comment='Has this data been processed into a generic serialization format?'),
    sa.Column('title', sa.String(length=500), nullable=True, comment='The title of the page to be displayed to the end user'),
    sa.Column('their_publish_date', sa.Date(), nullable=True, comment='The extracted date from their page that this was published on'),
    sa.Column('raw_body', sa.LargeBinary(), nullable=False, comment='The raw HTML response body in bytes'),
    sa.Column('string_body', sa.Text(), nullable=False, comment='The raw HTML body stringified'),
    sa.Column('body_data_block', sa.Text(), nullable=True, comment='The body block to be processed'),
    sa.Column('cleaned_data_block', sa.Text(), nullable=True, comment='Preprocessed content'),
    sa.Column('processed_data_block', sa.Text(), nullable=True, comment='The processed Markdown body'),
    sa.Column('processed', sa.Boolean(), nullable=False, comment='Has this data been processed into a generic serialization format?'),
    sa.Column('job_id', sa.Integer(), nullable=False),
    sa.Column('party_id', sa.Integer(), nullable=False),
    sa.Column('region_id', sa.Integer(), nullable=False),
    sa.Column('href_id', sa.Integer(), nullable=False),
    sa.Column('resource_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['href_id'], ['href.id'], name=op.f('fk_item_href_id_href')),
    sa.ForeignKeyConstraint(['job_id'], ['job.id'], name=op.f('fk_item_job_id_job')),
    sa.ForeignKeyConstraint(['party_id'], ['party.id'], name=op.f('fk_item_party_id_party')),
    sa.ForeignKeyConstraint(['region_id'], ['region.id'], name=op.f('fk_item_region_id_region')),
    sa.ForeignKeyConstraint(['resource_id'], ['resource.id'], name=op.f('fk_item_resource_id_resource')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_item'))
    )
    op.create_index(op.f('ix_item_date_created'), 'item', ['date_created'], unique=False)
    op.create_index(op.f('ix_item_date_modified'), 'item', ['date_modified'], unique=False)
    op.create_index(op.f('ix_item_encoding'), 'item', ['encoding'], unique=False)
    op.create_index(op.f('ix_item_href_id'), 'item', ['href_id'], unique=False)
    op.create_index(op.f('ix_item_id'), 'item', ['id'], unique=True)
    op.create_index(op.f('ix_item_identifier'), 'item', ['identifier'], unique=True)
    op.create_index(op.f('ix_item_job_id'), 'item', ['job_id'], unique=False)
    op.create_index(op.f('ix_item_party_id'), 'item', ['party_id'], unique=False)
    op.create_index(op.f('ix_item_processed'), 'item', ['processed'], unique=False)
    op.create_index(op.f('ix_item_region_id'), 'item', ['region_id'], unique=False)
    op.create_index(op.f('ix_item_resource_id'), 'item', ['resource_id'], unique=False)
    op.create_index(op.f('ix_item_their_publish_date'), 'item', ['their_publish_date'], unique=False)
    op.create_index(op.f('ix_item_title'), 'item', ['title'], unique=False)
    op.create_table('item_member_through',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('item_id', sa.Integer(), nullable=False),
    sa.Column('member_id', sa.Integer(), nullable=False),
    sa.Column('resource_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['item_id'], ['item.id'], name=op.f('fk_item_member_through_item_id_item')),
    sa.ForeignKeyConstraint(['member_id'], ['member.id'], name=op.f('fk_item_member_through_member_id_member')),
    sa.ForeignKeyConstraint(['resource_id'], ['resource.id'], name=op.f('fk_item_member_through_resource_id_resource')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_item_member_through')),
    sa.UniqueConstraint('item_id', 'member_id', name='uix_item_id_member_id_item_member_through')
    )
    op.create_index('item_id', 'item_member_through', ['member_id'], unique=False)
    op.create_index(op.f('ix_item_member_through_date_created'), 'item_member_through', ['date_created'], unique=False)
    op.create_index(op.f('ix_item_member_through_date_modified'), 'item_member_through', ['date_modified'], unique=False)
    op.create_index(op.f('ix_item_member_through_id'), 'item_member_through', ['id'], unique=True)
    op.create_index(op.f('ix_item_member_through_identifier'), 'item_member_through', ['identifier'], unique=True)
    op.create_index(op.f('ix_item_member_through_item_id'), 'item_member_through', ['item_id'], unique=False)
    op.create_index(op.f('ix_item_member_through_member_id'), 'item_member_through', ['member_id'], unique=False)
    op.create_index(op.f('ix_item_member_through_resource_id'), 'item_member_through', ['resource_id'], unique=False)
    op.create_index('member_id', 'item_member_through', ['item_id'], unique=False)
    op.create_table('itemcomponent',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('identifier', postgresql.UUID(as_uuid=True), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('date_created', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('date_modified', postgresql.TIMESTAMP(), server_default=sa.text('now()'), nullable=False),
    sa.Column('item_id', sa.Integer(), nullable=False),
    sa.Column('job_id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=50), nullable=False),
    sa.Column('content', sa.Text(), nullable=False),
    sa.Column('resource_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['item_id'], ['item.id'], name=op.f('fk_itemcomponent_item_id_item')),
    sa.ForeignKeyConstraint(['job_id'], ['job.id'], name=op.f('fk_itemcomponent_job_id_job')),
    sa.ForeignKeyConstraint(['resource_id'], ['resource.id'], name=op.f('fk_itemcomponent_resource_id_resource')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_itemcomponent'))
    )
    op.create_index(op.f('ix_itemcomponent_content'), 'itemcomponent', ['content'], unique=False)
    op.create_index(op.f('ix_itemcomponent_date_created'), 'itemcomponent', ['date_created'], unique=False)
    op.create_index(op.f('ix_itemcomponent_date_modified'), 'itemcomponent', ['date_modified'], unique=False)
    op.create_index(op.f('ix_itemcomponent_id'), 'itemcomponent', ['id'], unique=True)
    op.create_index(op.f('ix_itemcomponent_identifier'), 'itemcomponent', ['identifier'], unique=True)
    op.create_index(op.f('ix_itemcomponent_item_id'), 'itemcomponent', ['item_id'], unique=False)
    op.create_index(op.f('ix_itemcomponent_job_id'), 'itemcomponent', ['job_id'], unique=False)
    op.create_index(op.f('ix_itemcomponent_name'), 'itemcomponent', ['name'], unique=False)
    op.create_index(op.f('ix_itemcomponent_resource_id'), 'itemcomponent', ['resource_id'], unique=False)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_itemcomponent_resource_id'), table_name='itemcomponent')
    op.drop_index(op.f('ix_itemcomponent_name'), table_name='itemcomponent')
    op.drop_index(op.f('ix_itemcomponent_job_id'), table_name='itemcomponent')
    op.drop_index(op.f('ix_itemcomponent_item_id'), table_name='itemcomponent')
    op.drop_index(op.f('ix_itemcomponent_identifier'), table_name='itemcomponent')
    op.drop_index(op.f('ix_itemcomponent_id'), table_name='itemcomponent')
    op.drop_index(op.f('ix_itemcomponent_date_modified'), table_name='itemcomponent')
    op.drop_index(op.f('ix_itemcomponent_date_created'), table_name='itemcomponent')
    op.drop_index(op.f('ix_itemcomponent_content'), table_name='itemcomponent')
    op.drop_table('itemcomponent')
    op.drop_index('member_id', table_name='item_member_through')
    op.drop_index(op.f('ix_item_member_through_resource_id'), table_name='item_member_through')
    op.drop_index(op.f('ix_item_member_through_member_id'), table_name='item_member_through')
    op.drop_index(op.f('ix_item_member_through_item_id'), table_name='item_member_through')
    op.drop_index(op.f('ix_item_member_through_identifier'), table_name='item_member_through')
    op.drop_index(op.f('ix_item_member_through_id'), table_name='item_member_through')
    op.drop_index(op.f('ix_item_member_through_date_modified'), table_name='item_member_through')
    op.drop_index(op.f('ix_item_member_through_date_created'), table_name='item_member_through')
    op.drop_index('item_id', table_name='item_member_through')
    op.drop_table('item_member_through')
    op.drop_index(op.f('ix_item_title'), table_name='item')
    op.drop_index(op.f('ix_item_their_publish_date'), table_name='item')
    op.drop_index(op.f('ix_item_resource_id'), table_name='item')
    op.drop_index(op.f('ix_item_region_id'), table_name='item')
    op.drop_index(op.f('ix_item_processed'), table_name='item')
    op.drop_index(op.f('ix_item_party_id'), table_name='item')
    op.drop_index(op.f('ix_item_job_id'), table_name='item')
    op.drop_index(op.f('ix_item_identifier'), table_name='item')
    op.drop_index(op.f('ix_item_id'), table_name='item')
    op.drop_index(op.f('ix_item_href_id'), table_name='item')
    op.drop_index(op.f('ix_item_encoding'), table_name='item')
    op.drop_index(op.f('ix_item_date_modified'), table_name='item')
    op.drop_index(op.f('ix_item_date_created'), table_name='item')
    op.drop_table('item')
    op.drop_index(op.f('ix_href_url'), table_name='href')
    op.drop_index(op.f('ix_href_resource_id'), table_name='href')
    op.drop_index(op.f('ix_href_identifier'), table_name='href')
    op.drop_index(op.f('ix_href_id'), table_name='href')
    op.drop_index(op.f('ix_href_domain_id'), table_name='href')
    op.drop_index(op.f('ix_href_date_modified'), table_name='href')
    op.drop_index(op.f('ix_href_date_created'), table_name='href')
    op.drop_table('href')
    op.drop_index(op.f('ix_resource_history_resource_id'), table_name='resource_history')
    op.drop_index(op.f('ix_resource_history_resource_data'), table_name='resource_history')
    op.drop_index(op.f('ix_resource_history_identifier'), table_name='resource_history')
    op.drop_index(op.f('ix_resource_history_id'), table_name='resource_history')
    op.drop_index(op.f('ix_resource_history_date_modified'), table_name='resource_history')
    op.drop_index(op.f('ix_resource_history_date_created'), table_name='resource_history')
    op.drop_index(op.f('ix_resource_history_date_archived'), table_name='resource_history')
    op.drop_table('resource_history')
    op.drop_index(op.f('ix_region_resource_id'), table_name='region')
    op.drop_index(op.f('ix_region_identifier'), table_name='region')
    op.drop_index(op.f('ix_region_id'), table_name='region')
    op.drop_index(op.f('ix_region_date_modified'), table_name='region')
    op.drop_index(op.f('ix_region_date_created'), table_name='region')
    op.drop_table('region')
    op.drop_index(op.f('ix_party_resource_id'), table_name='party')
    op.drop_index(op.f('ix_party_name'), table_name='party')
    op.drop_index(op.f('ix_party_identifier'), table_name='party')
    op.drop_index(op.f('ix_party_id'), table_name='party')
    op.drop_index(op.f('ix_party_date_modified'), table_name='party')
    op.drop_index(op.f('ix_party_date_created'), table_name='party')
    op.drop_table('party')
    op.drop_index(op.f('ix_member_resource_id'), table_name='member')
    op.drop_index(op.f('ix_member_last_name'), table_name='member')
    op.drop_index(op.f('ix_member_identifier'), table_name='member')
    op.drop_index(op.f('ix_member_id'), table_name='member')
    op.drop_index(op.f('ix_member_first_name'), table_name='member')
    op.drop_index(op.f('ix_member_date_of_birth'), table_name='member')
    op.drop_index(op.f('ix_member_date_modified'), table_name='member')
    op.drop_index(op.f('ix_member_date_created'), table_name='member')
    op.drop_table('member')
    op.drop_index(op.f('ix_job_start_time'), table_name='job')
    op.drop_index(op.f('ix_job_spider_name'), table_name='job')
    op.drop_index(op.f('ix_job_resource_id'), table_name='job')
    op.drop_index(op.f('ix_job_identifier'), table_name='job')
    op.drop_index(op.f('ix_job_id'), table_name='job')
    op.drop_index(op.f('ix_job_finish_time'), table_name='job')
    op.drop_index(op.f('ix_job_date_modified'), table_name='job')
    op.drop_index(op.f('ix_job_date_created'), table_name='job')
    op.drop_table('job')
    op.drop_index(op.f('ix_domain_suffix'), table_name='domain')
    op.drop_index(op.f('ix_domain_subdomain'), table_name='domain')
    op.drop_index(op.f('ix_domain_resource_id'), table_name='domain')
    op.drop_index(op.f('ix_domain_identifier'), table_name='domain')
    op.drop_index(op.f('ix_domain_id'), table_name='domain')
    op.drop_index(op.f('ix_domain_domain'), table_name='domain')
    op.drop_index(op.f('ix_domain_date_modified'), table_name='domain')
    op.drop_index(op.f('ix_domain_date_created'), table_name='domain')
    op.drop_table('domain')
    op.drop_index(op.f('ix_resource_resource_type_id'), table_name='resource')
    op.drop_index(op.f('ix_resource_is_archived'), table_name='resource')
    op.drop_index(op.f('ix_resource_identifier'), table_name='resource')
    op.drop_index(op.f('ix_resource_id'), table_name='resource')
    op.drop_index(op.f('ix_resource_date_modified'), table_name='resource')
    op.drop_index(op.f('ix_resource_date_created'), table_name='resource')
    op.drop_index(op.f('ix_resource_date_archived'), table_name='resource')
    op.drop_table('resource')
    op.drop_index(op.f('ix_resource_type_name'), table_name='resource_type')
    op.drop_index(op.f('ix_resource_type_identifier'), table_name='resource_type')
    op.drop_index(op.f('ix_resource_type_id'), table_name='resource_type')
    op.drop_index(op.f('ix_resource_type_date_modified'), table_name='resource_type')
    op.drop_index(op.f('ix_resource_type_date_created'), table_name='resource_type')
    op.drop_table('resource_type')
    # ### end Alembic commands ###
