"""create account table

Revision ID: 1d30b8fb9455
Revises: 
Create Date: 2022-08-30 07:50:31.020195

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1d30b8fb9455'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
