"""create account table2

Revision ID: df5961f87b76
Revises: 1d30b8fb9455
Create Date: 2022-08-30 07:54:50.700318

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'df5961f87b76'
down_revision = '1d30b8fb9455'
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
