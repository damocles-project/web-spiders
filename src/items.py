# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader import ItemLoader


class ResponseItem(scrapy.Item):
    
    def __init__(self, response, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.response = response
    
    title = scrapy.Field()
    encoding = scrapy.Field()
    raw_body = scrapy.Field()
    string_body = scrapy.Field()
    body_data_block = scrapy.Field()
    cleaned_data_block = scrapy.Field()
    processed_data_block = scrapy.Field()
    processed = scrapy.Field()
    their_publish_date = scrapy.Field()
    
    party_id = scrapy.Field()
    region_id = scrapy.Field()
    job_id = scrapy.Field()
    href_id = scrapy.Field()
    
    
class ResponseExtractor(ItemLoader):
    
    @staticmethod
    def parse(item, context):
        import ipdb; ipdb.set_trace()