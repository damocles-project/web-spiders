import pypandoc
import bs4
from src.spiders import (
    BaseWebsiteSpider,
)

from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'liberals-wa'
    party_identifier = Parties.LIBERALS
    region_identifier = Regions.WA
    
    allowed_domains = (
        'waliberal.org.au',
    )
    start_urls = (
        'https://www.waliberal.org.au/',
    )
