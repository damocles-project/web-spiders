from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'liberals-sa'
    party_identifier = Parties.LIBERALS
    region_identifier = Regions.SA
    
    allowed_domains = (
        'saliberal.org.au',
    )
    start_urls = (
        'https://www.saliberal.org.au/',
    )
    