from spiders.base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'liberals-vic'
    party_identifier = Parties.LIBERALS
    region_identifier = Regions.VIC
    
    allowed_domains = (
        'vic.liberal.org.au',
    )
    start_urls = (
        'https://vic.liberal.org.au/',
    )
    