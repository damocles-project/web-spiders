from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'liberals-nsw'
    party_identifier = Parties.LIBERALS
    region_identifier = Regions.NSW
    
    allowed_domains = (
        'nsw.liberal.org.au',
    )
    start_urls = (
        'https://nsw.liberal.org.au/',
    )
    