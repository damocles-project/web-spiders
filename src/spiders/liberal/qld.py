from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'liberals-qld'
    party_identifier = Parties.LIBERALS
    region_identifier = Regions.QLD
    
    allowed_domains = (
        'lnp.org.au',
    )
    start_urls = (
        'https://www.lnp.org.au/',
    )
    