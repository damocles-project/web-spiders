from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor
from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'liberals-fed'
    party_identifier = Parties.LIBERALS
    region_identifier = Regions.FEDERAL

    rules = (Rule(LinkExtractor(allow=(r'^https?://www.liberal.org.au/.*',))),)
    
    allowed_domains = (
        'www.liberal.org.au',
    )
    start_urls = (
        'https://www.liberal.org.au/',
    )
    