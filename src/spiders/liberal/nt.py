from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'liberals-nt'
    party_identifier = Parties.LIBERALS
    region_identifier = Regions.NT
    
    allowed_domains = (
        'countryliberal.org',
    )
    start_urls = (
        'http://www.countryliberal.org/',
    )
    