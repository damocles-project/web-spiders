from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'liberals-act'
    party_identifier = Parties.LIBERALS
    region_identifier = Regions.ACT
    
    allowed_domains = (
        'canberraliberals.org.au',
    )
    start_urls = (
        'http://canberraliberals.org.au/',
    )
    