import bs4
from breadability.readable import Article

from src.spiders import (
    BaseWebsiteSpider,
)
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'liberals-tas'
    party_identifier = Parties.LIBERALS
    region_identifier = Regions.TAS
    
    allowed_domains = (
        'tas.liberal.org.au',
    )
    start_urls = (
        'https://www.tas.liberal.org.au/policy',
    )
