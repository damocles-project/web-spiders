from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'labor-act'
    party_identifier = Parties.LABOR
    region_identifier = Regions.ACT
    
    allowed_domains = (
        'actlabor.org.au',
    )
    start_urls = (
        'http://www.actlabor.org.au/',
    )
    