from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'labor-sa'
    party_identifier = Parties.LABOR
    region_identifier = Regions.SA
    
    allowed_domains = (
        'sa.alp.org.au',
    )
    start_urls = (
        'https://sa.alp.org.au/',
    )
    