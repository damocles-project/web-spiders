from base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'labor-vic'
    party_identifier = Parties.LABOR
    region_identifier = Regions.VIC
    
    allowed_domains = (
        'viclabor.com.au',
    )
    start_urls = (
        'https://www.viclabor.com.au/',
    )
    