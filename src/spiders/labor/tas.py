from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'labor-tas'
    party_identifier = Parties.LABOR
    region_identifier = Regions.TAS
    
    allowed_domains = (
        'taslabor.com',
        'puttingpeoplefirst.org.au'
    )
    start_urls = (
        'http://taslabor.com/',
        'https://www.puttingpeoplefirst.org.au/'
    )
    