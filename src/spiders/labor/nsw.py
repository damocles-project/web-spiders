from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'labor-nsw'
    party_identifier = Parties.LABOR
    region_identifier = Regions.NSW
    
    allowed_domains = (
        'nswlabor.org.au',
    )
    start_urls = (
        'http://www.nswlabor.org.au/sign_up?splash=1',
    )
    