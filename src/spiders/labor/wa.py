from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'labor-wa'
    party_identifier = Parties.LABOR
    region_identifier = Regions.WA
    
    allowed_domains = (
        'walabor.org.au',
    )
    start_urls = (
        'https://walabor.org.au/',
    )
    