from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor
from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'labor-fed'
    party_identifier = Parties.LABOR
    region_identifier = Regions.FEDERAL
    
    # will need to check the BaseExtractor regex patterns
    # rules = (Rule(LinkExtractor(allow=(r'^https?://www.alp.org.au/.*',))),)
    
    allowed_domains = (
        'alp.org.au',
        'theirfairshare.org.au'
    )
    start_urls = (
        'https://www.alp.org.au/',
        'www.theirfairshare.org.au',
    )
