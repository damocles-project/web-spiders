from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from .act import Spider as ACTSpider
from .nsw import Spider as NSWSpider
from .nt import Spider as NTSpider
from .qld import Spider as QLDSpider
from .sa import Spider as SASpider
from .tas import Spider as TASSpider
from .wa import Spider as WASpider
from .federal import Spider as FederalSpider


def start_process():
    process = CrawlerProcess(get_project_settings())
    process.crawl(ACTSpider)
    process.crawl(NSWSpider)
    process.crawl(NTSpider)
    process.crawl(QLDSpider)
    process.crawl(SASpider)
    process.crawl(TASSpider)
    process.crawl(WASpider)
    process.crawl(FederalSpider)
    
    process.start()
