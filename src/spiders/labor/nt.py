from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'labor-nt'
    party_identifier = Parties.LABOR
    region_identifier = Regions.NT
    
    allowed_domains = (
        'territorylabor.com.au',
    )
    start_urls = (
        'http://territorylabor.com.au/',
    )
    