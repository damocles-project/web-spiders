from ..base import BaseWebsiteSpider
from src.constants import (
    Parties,
    Regions,
)


class Spider(BaseWebsiteSpider):
    name = 'labor-qld'
    party_identifier = Parties.LABOR
    region_identifier = Regions.QLD
    
    allowed_domains = (
        'queenslandlabor.org',
    )
    start_urls = (
        'https://www.queenslandlabor.org/',
    )
    