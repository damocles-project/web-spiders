import scrapy
import tldextract
import json
import furl

from scrapy import signals
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from pydispatch import dispatcher

from src.schema.models.locations import Region
from src.schema.connectors.postgres import Session
from src.utils.sqlalchemy import get_one_or_create
from src.schema.models.agents import Party
from src.schema.models.items import (
    Job,
    HREF,
    Domain,
)

from src.extractors import DefaultExtractor


class ResponseItem(scrapy.Item):
    def method(self):
        pass
    
    response = scrapy.Field()
    
    image_urls = scrapy.Field()
    images = scrapy.Field()
    
    file_urls = scrapy.Field()
    files = scrapy.Field()
    
    title = scrapy.Field()
    encoding = scrapy.Field()
    raw_body = scrapy.Field()
    string_body = scrapy.Field()
    body_data_block = scrapy.Field()
    cleaned_data_block = scrapy.Field()
    processed_data_block = scrapy.Field()
    processed = scrapy.Field()
    their_publish_date = scrapy.Field()
    
    party_id = scrapy.Field()
    region_id = scrapy.Field()
    job_id = scrapy.Field()
    href_id = scrapy.Field()


class BaseWebsiteSpider(CrawlSpider):
    """
    Subclass all standard website spiders from this.
    """
    party_identifier = None
    region_identifier = None
    extractor = DefaultExtractor
    
    # dont override these, all built at runtime
    _job = None
    _party = None
    _region = None
    _domain = None
    session = Session()
    
    rules = (Rule(LinkExtractor(allow=()), callback='process_response', follow=True),)
    
    def __init__(self, *a, **kw):
        # setup
        super().__init__(*a, **kw)
        
        # safety
        if self.party_identifier is None:
            raise ValueError('`party_identifier` must be specified on all subclasses of BaseWebsiteSpider')
        
        if self.region_identifier is None:
            raise ValueError('`region_identifier` must be specified on all subclasses of BaseWebsiteSpider')
        
        # bind shutdown context into method hooks
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        
        # run database setup
        self._party, _ = self.get_party(self.party_identifier)
        self._region, _ = self.get_region(self.region_identifier)
        self._job = self.create_job()
    
    def create_job(self):
        job = Job(spider_name=self.name)
        self.session.add(job)
        return job
    
    def get_party(self, identifier):
        """Return the database record to link these posts too"""
        return get_one_or_create(
            session=self.session,
            model=Party,
            name=identifier,
        )
    
    def get_region(self, identifier):
        """Return the database record to link these posts too"""
        return get_one_or_create(
            session=self.session,
            model=Region,
            name=identifier,
        )
    
    def get_href(self, response):
        href, _ = get_one_or_create(
            session=self.session,
            model=HREF,
            url=response.url,
            domain_id=self.get_domain(response).id,
        )
        return href
    
    def get_domain(self, response):
        components = tldextract.extract(response.url)
        domain, _ = get_one_or_create(
            session=self.session,
            model=Domain,
            subdomain=components.subdomain,
            domain=components.domain,
            suffix=components.suffix,
        )
        return domain
    
    def get_item(self, response):
        item = ResponseItem(response=response)
        item['image_urls'] = self.process_images(response)
        item['file_urls'] = self.process_files(response)
        return item
    
    def process_response(self, response):
        self.get_domain(response)
        item = self.get_item(response)
        return item
    
    def process_images(self, response):
        base = furl.furl(response.url)
        images = [furl.furl(img.extract()) for img in response.xpath('//img[@src]/@src')]
        for image in images:
            if image.scheme is None:
                image.scheme = base.scheme
            
            if image.host is None:
                image.host = base.host
        return [str(image) for image in images]
    
    def process_files(self, response):
        return [file.extract() for file in response.xpath("//a[contains(@href,'.pdf')]/@href")]
    
    def write_job_stats(self):
        self._job.start_time = self.crawler.stats.get_stats()['start_time']
        self._job.finish_time = self.crawler.stats.get_stats()['finish_time']
        self._job.info = json.dumps(obj=self.crawler.stats.get_stats(), default=str, indent=4, sort_keys=True, )
    
    def spider_closed(self, spider):
        self.write_job_stats()
        self.session.commit()
        self.session.close()
