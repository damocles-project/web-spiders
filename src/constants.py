from types import SimpleNamespace


class Parties(SimpleNamespace):
    
    LIBERALS = 'Liberals'
    LABOR = 'Labor'
    GREENS = 'Greens'
    
    INDEPENDENT = 'Independent'
    
    
class Regions(SimpleNamespace):
    NSW = 'New South WaleAs'
    ACT = 'Australian Capital Territory'
    NT = 'Northern Territory'
    WA = 'Western Australia'
    VIC = 'Victoria'
    QLD = 'Queensland'
    TAS = 'Tasmania'
    SA = 'South Australia'
    
    FEDERAL = 'Federal'
    
    
ALLOWED_TAGS = [
    'a',
    'abbr',
    'acronym',
    'b',
    'blockquote',
    'code',
    'em',
    'i',
    'li',
    'ol',
    'strong',
    'ul',
    
    'table',
    'td'
    'tr',
    'tbody',
    'thead',
    
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
]