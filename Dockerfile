FROM python:3.10.6-bullseye

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

RUN apt-get install libxml2-dev libxslt-dev libjpeg-dev zlib1g-dev libpng-dev libpq-dev

RUN curl -sSL https://install.python-poetry.org | python -
ENV PATH="${PATH}:/root/.local/bin"
RUN poetry config virtualenvs.create false 

WORKDIR /etc/app/src

COPY pyproject.toml poetry.lock ./

RUN poetry install
