# Spiders

## Setup

- Build artifacts: `docker-compose up`
- Migrate DB: `docker exec -it spiders-app alembic upgrade head`


## Notes

TODO: Check ref implementation for async session scoping
https://www.patrick-muehlbauer.com/articles/fastapi-with-sqlalchemy
(I'm not convinced my session will be deloaded from the active thread, check when its not 3:50am in the morning)

TODO: Yield to postgres async driver and use those magic cooperative threads + greenlet

TODO: Assume that this will have some dAtA sCiEnTiSt hacks want to investigate the results, meaning:
- implement a messaging pub/sub system (Kafka or Pulsar)
- use row level replica logs to shit the data out into dbezium
- pipe dbezium into a kafka/pulsar cluster


  